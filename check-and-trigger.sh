#!/bin/bash

if [ -n "${CI_COMMIT_SHA}" ]; then
  # shellcheck disable=SC2034
  SRCREV_ltp="${CI_COMMIT_SHA}"

  # shellcheck disable=SC2034
  PUB_DEST="s3://storage.lkft.org/rootfs.ltp/oe-sumo/${CI_COMMIT_SHA:0:12}"
fi

BUILD_ID="$(git describe --always)"

# shellcheck disable=SC2034
MANIFEST_URL="https://gitlab.com/Linaro/lkft/rootfs/lkft-manifest.git"
# shellcheck disable=SC2034
MANIFEST_BRANCH="sumo"

for var in \
  BUILD_ID \
  MANIFEST_BRANCH \
  MANIFEST_URL \
  PUB_DEST \
  SRCREV_ltp; do
  echo -e "$var=${!var}" >> inputs.sh
done

echo "-----v-----v-----v-----"
cat inputs.sh
echo "-----^-----^-----^-----"
