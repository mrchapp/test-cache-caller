#!/bin/bash

ROOTDIR="$(readlink -e "$(dirname "$0")")"
CURL="curl --silent"

if [ $# -gt 0 ]; then
  DEVICE="$1"
fi

if [ -z "${DEVICE}" ]; then
  echo "No DEVICE is defined. What am I looking for?"
  exit 1
fi

if [ -z "${KERNEL_REPO}" ]; then
  KERNEL_REPO="https://gitlab.com/Linaro/lkft/mirrors/stable/linux-stable-rc.git"
fi
project="${KERNEL_REPO#https://gitlab.com/*}"
project="${project%*.git}"

if [ -n "${LATEST_RELEASE_PIPELINE_ID}" ]; then
  pipeline_id="${LATEST_RELEASE_PIPELINE_ID}"
else
  pipeline_id="$("${ROOTDIR}/get-latest-pipeline.sh")"
fi

echo "Looking for latest build artifacts for ${DEVICE} in #${pipeline_id}..."

# Find the build job's name for the DEVICE,
# as defined in the pipeline's tests.yml
curl -sSL \
  -o tests.yml \
  https://gitlab.com/Linaro/lkft/pipelines/lkft-common/-/raw/master/tests.yml
test_job_name="test-${DEVICE}"
build_job_name="$(yq -r ".[\"${test_job_name}\"] | .needs[0]" tests.yml)"
rm tests.yml

# Look for the build job in latest stable branch pipeline
: > jobs.json
project_id="${project//\//%2F}"
project_url="https://gitlab.com/api/v4/projects/${project_id}"
page=1
while true; do
  echo "Page ${page}..."
  # shellcheck disable=SC2068
  ${CURL[@]} -D headers.txt "${project_url}/pipelines/${pipeline_id}/jobs?per_page=100&page=${page}" >> jobs.json
  if ! grep ^link: headers.txt | grep -q next; then
    break
  fi
  page=$((page + 1))
done
build_job_id="$(jq -r ".[] | select(.name == \"${build_job_name}\") | .id" jobs.json)"
rm headers.txt
rm jobs.json

# Download artifacts for the build job
# shellcheck disable=SC2068
${CURL[@]} --output artifacts.zip -L "${project_url}/jobs/${build_job_id}/artifacts"
unzip artifacts.zip build.json
rm artifacts.zip
